#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>
#include <string>
#include <map>
#include <vector>
#include <utility>
#include <string>
using namespace cv;
using namespace std;
struct BGR{
    int b;
    int g;
    int r;
};

void contours(Mat* frame,Mat* original){
        Mat gray;
        cvtColor((*frame), gray, CV_BGR2GRAY);
        Canny(gray, gray, 100, 200, 3);
        /// Find contours
        vector<vector<Point> > contours;
        vector<Vec4i> hierarchy;
        RNG rng(12345);
        findContours( gray, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0) );
        // Draw contours

        Mat drawing = Mat::zeros( gray.size(), CV_8UC3 );
        for( int i = 0; i< contours.size(); i++ )
        {
            //Scalar color = Scalar( rng.uniform(0, 255), rng.uniform(0,255), rng.uniform(0,255) );
            drawContours( (*original), contours, i,Scalar(255,255,255));
            //rectangle( (*original), boundRect[i].tl(), boundRect[i].br(), Scalar(255,255,255), 2, 8, 0 );

        }

        //imshow( "Result window", (*original) );

}

void using_RGB(Mat *frame){

    //cout<<"new func\n";
    map<pair<int,pair<int,int>>,int> histogram;
    //string temp;
    for(int i=0; i<(*frame).rows; i++)
    {
        for(int j=0; j<(*frame).cols; j++)
            {

                pair<int,pair<int,int>> temp;
                temp.first=(int)((*frame).at<Vec3b>(i,j)[0]);
                temp.second.first=(int)((*frame).at<Vec3b>(i,j)[1]);
                temp.second.second=(int)((*frame).at<Vec3b>(i,j)[2]);
                // cout<<temp<<endl;
                histogram[temp]++;
            }
    }
     //cout<<"using map "<<histogram.size()<<endl;


     int max=0;
     //string color;
     pair<int,pair<int,int>> color;
     for (std::map<pair<int,pair<int,int>>,int>::iterator it=histogram.begin(); it!=histogram.end(); ++it)
           {
             if(it->second>max){
                 max=it->second;
                 color=it->first;
             }
           }

     int g,b,r;
     int i=0;
     b=color.first;
     g=color.second.first;
     r=color.second.second;
     //cout<<"max is "<<max<<" color is "<<b<<" "<<g<<" "<<r<<"\n";
     BGR res;
     res.b=b;
     res.g=g;
     res.r=r;


     int range=40;
     Mat mask((*frame).rows,(*frame).cols,CV_8UC3,Scalar(0,0,0));
     for(int i=0; i<mask.rows; i++)
         for(int j=0; j<mask.cols; j++)
          {
             if((*frame).at<Vec3b>(i,j)[0]>res.b-range&&(*frame).at<Vec3b>(i,j)[0]<res.b+range
                &&(*frame).at<Vec3b>(i,j)[1]>res.g-range&&(*frame).at<Vec3b>(i,j)[1]<res.g+range
                &&(*frame).at<Vec3b>(i,j)[2]>res.r-range&&(*frame).at<Vec3b>(i,j)[2]<res.r+range)
             {
                 mask.at<Vec3b>(i,j)[0]=1;
                 mask.at<Vec3b>(i,j)[1]=1;
                 mask.at<Vec3b>(i,j)[2]=1;
             }

         }

     Mat output = (*frame).mul(mask);
     //imshow( "original",image);
     //imshow("out",output);
     contours(&output,&(*frame));

}
/*void using_hsv(Mat *frame){

    Mat image_hsv;
    cvtColor((*frame), image_hsv,COLOR_BGR2HSV);
    map<int,int> histogram;
    int temp;
    for(int i=0; i<image_hsv.rows; i++)
    {
        for(int j=0; j<image_hsv.cols; j++)
            {

                temp=(int)image_hsv.at<Vec3b>(i,j)[0];
                histogram[temp]++;
            }
    }
     //cout<<"using map "<<histogram.size()<<endl;


     int max=0;
     int color;
     for (std::map<int,int>::iterator it=histogram.begin(); it!=histogram.end(); ++it)
           {
             if(it->second>max){
                 max=it->second;
                 color=it->first;
             }
           }


     int range=50;
     Mat mask((*frame).rows,(*frame).cols,CV_8UC3,Scalar(0,0,0));
     for(int i=0; i<mask.rows; i++)
         for(int j=0; j<mask.cols; j++)
          {
             if(image_hsv.at<Vec3b>(i,j)[0]>color-range&&image_hsv.at<Vec3b>(i,j)[0]<color+range)
             {
                 mask.at<Vec3b>(i,j)[0]=1;
                 mask.at<Vec3b>(i,j)[1]=1;
                 mask.at<Vec3b>(i,j)[2]=1;
             }

         }

     Mat output = (*frame).mul(mask);
     //imshow( "original",image);
     //imshow("out",output);
     contours(&output,&(*frame));

}*/
int main()
{

    cout<<"enter video name\n";
    string name;
    cin>>name;
    VideoCapture cap(name);
    VideoWriter video("out_"+name+".avi",CV_FOURCC('H','2','6','4'),10, Size(cap.get(CV_CAP_PROP_FRAME_WIDTH),cap.get(CV_CAP_PROP_FRAME_HEIGHT)));

    if(!cap.isOpened())
      {
          cout << "Error can't open the file"<<endl;
          return -1;
      }
    int i=0;
    Mat image;
    while(1){


        // Capture frame-by-frame
        cap >> image;

        // If the frame is empty, break immediately
        if (image.empty())
          break;

        // Display the resulting frame
        //imshow( "Frame", frame );
         using_RGB(&image);
         //using_hsv(&image);
         imshow("output",image);
         //video.write(image);
        cout<<i++<<"\n";
        // Press  ESC on keyboard to exit
        char c=(char)waitKey(25);
        if(c==27)
          break;
      }
   // BGR res=using_map(&image);
    cout<<"finish\n";
    cap.release();
    video.release();

    return 0;
}
