TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp
INCLUDEPATH +=/home/bishoy/Documents/opencv-3.4.1/build/include

LIBS += -L"/home/bishoy/Documents/opencv-3.4.1/build/lib"

LIBS += -lopencv_calib3d

LIBS += -lopencv_core

LIBS += -lopencv_features2d

LIBS += -lopencv_flann

LIBS += -lopencv_highgui

LIBS += -lopencv_imgcodecs

LIBS += -lopencv_imgproc

LIBS += -lopencv_ml

LIBS += -lopencv_objdetect

LIBS += -lopencv_photo

LIBS += -lopencv_shape

LIBS += -lopencv_stitching

LIBS += -lopencv_superres

LIBS += -lopencv_video

LIBS += -lopencv_videoio

LIBS += -lopencv_videostab

